// Package linkedin provides a Go abstraction around the LinkedIn API.
//
// It is not intended to be a complete implementation, as all I need it for is
// creating posts and comments.  I may expand the functionality as needs
// dictate.
package linkedin
