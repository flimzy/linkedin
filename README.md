[![Go Report Card](https://goreportcard.com/badge/gitlab.com/flimzy/linkedin)](https://goreportcard.com/report/gitlab.com/flimzy/linkedin)
[![GoDoc](https://godoc.org/gitlab.com/flimzy/linkedin?status.svg)](https://pkg.go.dev/gitlab.com/flimzy/linkedin)

# LinkedIn Go SDK
